(ns day20
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day20_input.txt")))

(def line-regex #"p=<(-?\d+),(-?\d+),(-?\d+)>, v=<(-?\d+),(-?\d+),(-?\d+)>, a=<(-?\d+),(-?\d+),(-?\d+)>")

(defn parse-input-line [line index]
  (let [[_ px py pz vx vy vz ax ay az] (re-matches line-regex line)]
    {:index index
     :pos   (mapv parse-int [px py pz])
     :vel   (mapv parse-int [vx vy vz])
     :acc   (mapv parse-int [ax ay az])}))

(def input (map parse-input-line (str/split-lines input-string) (range)))

(defn mag [vec]
  (Math/sqrt (apply + (map * vec vec))))

(defn part1 []
  (:index
    (first
      (sort-by
        (comp mag :acc)
        input))))

(defn update-particle [particle]
  (let [{:keys [:pos :vel :acc]} particle
        vel (mapv + vel acc)
        pos (mapv + pos vel)]
    (assoc particle :pos pos :vel vel)))

(defn tick-system [system]
  (let [updated (map update-particle system)
        grouped (group-by :pos updated)
        system (vec
                 (for [[_ grp] grouped
                       :when (= 1 (count grp))]
                   (first grp)))]
    system))

(defn part2 []
  (count
    (nth
      (iterate tick-system input)
      10000)))