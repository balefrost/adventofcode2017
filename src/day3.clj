(ns day3
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input 312051)

(def distance-pattern
  (mapcat
    (fn [x] [x x])
    (iterate inc 1)))

(def north [0 1])
(def east [1 0])
(def south [0 -1])
(def west [-1 0])

(def direction-pattern
  (cycle [east north west south]))

(def pattern
  (mapcat
    (fn [distance direction]
      (repeat distance direction))
    distance-pattern
    direction-pattern))

(defn step-pos [current-pos dir]
  (mapv + current-pos dir))

(def locations
  (reductions
    step-pos
    [0 0]
    pattern))

(defn abs [n]
  (max n (- n)))

(defn part1 []
  (let [[x y] (nth locations (dec input))]
    (+ (abs x) (abs y))))

(defn neighbors [location]
  (for [dx [-1 0 1]
        dy [-1 0 1]]
    (mapv + location [dx dy])))

(defn compute-cell-value [cells location]
  (apply +
         (for [n (neighbors location)]
           (get cells n 0))))

(defn write-cell [cells location]
  (let [cell-value (compute-cell-value cells location)]
    (assoc cells location cell-value)))

(defn cell-value-helper [cells idx locations]
  (lazy-seq
    (let [[location & locations] locations
          cell-value (compute-cell-value cells location)
          cells1 (assoc cells location cell-value)]
      (cons cell-value
            (cell-value-helper
              cells1
              (inc idx)
              locations)))))

(def cell-values
  (cell-value-helper {[0 0] 1} 2 locations))

(defn part2 []
  (first (drop-while #(<= % input) cell-values)))