(ns day19
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day19_input.txt")))

(def input
  (mapv
    vec
    (str/split-lines input-string)))

(defn turned-dirs [dir]
  (let [[dy dx] dir]
    [[dx dy]
     [(- dx) (- dy)]]))

(defn get-at [m coord]
  (get-in m coord \space))

(defn single [coll]
  (if-let [[hd & tl] coll]
    (if (empty? tl)
      hd)))

(defn walk [m]
  (letfn [(helper [prev-coord dir]
            (lazy-seq
              (let [coord (mapv + prev-coord dir)
                    value (get-at m coord)]
                (case value
                  \+ (let [turns (turned-dirs dir)
                           valid (single
                                   (filter
                                     #(not= \space (get-at m (mapv + coord %)))
                                     turns))]
                       (cons coord (helper coord valid)))
                  \space nil
                  (cons coord (helper coord dir))))))]
    (let [initial-coord [0 (index-of (first input) \|)]]
      (cons initial-coord (helper initial-coord [1 0])))))

(defn part1 []
  (let [coords (walk input)
        chars (map #(get-at input %) coords)
        letters (remove #{\| \- \+} chars)]
    (apply str letters)))

(defn part2 []
  (let [coords (walk input)]
    (count coords)))