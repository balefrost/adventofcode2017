(ns day12
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (str/trim (slurp (io/resource "day12_input.txt"))))

(def line-regex #"^(\d+) <-> (.*)$")

(defn parse-input-line [line]
  (let [[_ from to-string] (re-matches line-regex line)
        to (map advent-util/parse-int (str/split to-string #", "))]
    {:from (advent-util/parse-int from)
     :to to}))

(def input
  (->> input-string
       (str/split-lines)
       (map parse-input-line)
       (map (fn [entry] [(:from entry) (:to entry)]))
       (into {})))

(defn walk-from [id graph]
  (loop [seen #{}
         to-explore [id]]
    (if (empty? to-explore)
      seen
      (let [[hd & to-explore] to-explore]
        (if (seen hd)
          (recur
            seen
            to-explore)
          (recur
            (conj seen hd)
            (apply conj to-explore (graph hd))))))))

(defn part1 []
  (count (walk-from 0 input)))

(defn part2 []
  (loop [remaining (keys input)
         groups []]
    (if (empty? remaining)
      (count groups)
      (let [[hd & remaining] remaining]
        (if (some #(contains? % hd) groups)
          (recur remaining groups)
          (let [grp (walk-from hd input)]
            (recur
              remaining
              (conj groups grp))))))))