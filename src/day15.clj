(ns day15
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (str/trim (slurp (io/resource "day15_input.txt"))))

(def input-line-regex #"^Generator \w starts with (\d+)$")

(defn parse-input-line [line]
  (let [[_ start] (re-matches input-line-regex line)]
    (Integer/parseInt start)))

(let [[a b] (map parse-input-line (str/split-lines input-string))]
  (def gena-start a)
  (def genb-start b))

(def gena-factor 16807)
(def genb-factor 48271)

(def gena-multiple 4)
(def genb-multiple 8)

(defn iterate-generator [^long start ^long factor]
  (rest
    (iterate
      (fn [^long prev] (mod (* factor prev) 2147483647))
      start)))

(defn count-matches [limit seqa seqb]
  (apply +
         (take limit
               (pmap
                 (fn [^long idx ^long a ^long b]
                   (if (zero? (mod idx 1000000))
                     (println idx))
                   (let [alow (bit-and a 0xffff)
                         blow (bit-and b 0xffff)]
                     (if (= alow blow)
                       1
                       0)))
                 (range)
                 seqa
                 seqb))))

(defn part1 []
  (count-matches
    40000000
    (iterate-generator gena-start gena-factor)
    (iterate-generator genb-start genb-factor)))

(defn part2 []
  (count-matches
    5000000
    (filter
      (fn [^long v] (zero? (mod v gena-multiple)))
      (iterate-generator gena-start gena-factor))
    (filter
      (fn [^long v] (zero? (mod v genb-multiple)))
      (iterate-generator genb-start genb-factor))))
