(ns day17
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input 382)

(defn insert! [tcoll index value]
  (let [shifted (reduce
                  #(assoc! %1 %2 (%1 (dec %2)))
                  tcoll
                  (range (count tcoll) index -1))]
    (assoc! shifted index value)))

(defn insertion-seq [limit advance]
  (letfn [(helper [to-insert current-idx]
            (lazy-seq
              (if (<= to-insert limit)
                (let [new-idx (inc (mod (+ current-idx advance) to-insert))]
                  (cons [new-idx to-insert] (helper (inc to-insert) new-idx))))))]
    (helper 1 0)))

(defn perform-insertions [limit advance]
  (persistent!
    (reduce
      (fn [tcoll [idx v]]
        (insert! tcoll idx v))
      (transient [0])
      (insertion-seq limit advance))))

(defn part1 []
  (let [result (perform-insertions 2017 input)
        idx (index-of result 2017)]
    (result (inc idx))))

(defn part2 []
  (let [insertions (insertion-seq 50000000 input)
        relevant-insertions (filter
                              (fn [[idx v]] (= idx 1))
                              insertions)]
    (second (last relevant-insertions))))

