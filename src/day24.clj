(ns day24
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day24_input.txt")))

(defn parse-line [line]
  (let [pins (str/split line #"/")
        pins (map parse-int pins)]
    (vec (sort pins))))

(def input
  (map
    parse-line
    (str/split-lines input-string)))

(defn orient-component [prev-pin component]
  (let [[a b] component]
    (if (= a prev-pin)
      component
      [b a])))

(defn remove-first [coll item]
  (lazy-seq
    (if-let [[a & bs] (seq coll)]
      (if (= a item)
        bs
        (cons a (remove-first bs item))))))

(defn dec-or-delete [m k]
  (let [v (get m k)]
    (if (> v 1)
      (update m k dec)
      (dissoc m k))))

(defn histo [coll]
  (reduce
    (fn [acc item]
      (let [v (get acc item 0)]
        (assoc acc item (inc v))))
    {}
    coll))

(defn build-bridges [score length prev-pin components]
  (lazy-seq
    (cons
      {:score  score
       :length length}
      (if-not (empty? components)
        (mapcat
          (fn [[component count]]
            (if (some #{prev-pin} component)
              (let [[a b :as oriented] (orient-component prev-pin component)]
                (build-bridges
                  (+ score a b)
                  (inc length)
                  b
                  (dec-or-delete components component)))))
          components)))))

(defn part1 []
  (apply
    max
    (map
      :score
      (build-bridges 0 0 0 (histo input)))))

(defn part2 []
  (:score
    (first
      (sort-by
        #(vector (:length %) (:score %))
        #(compare %2 %1)
        (build-bridges 0 0 0 (histo input))))))
