(ns day23
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day23_input.txt")))

(defn parse-input-line [line]
  (let [[_ op args] (re-matches #"^(\w+?) (.*)$" line)
        args (re-seq #"[^ ]+" args)
        args (mapv #(if (re-matches #"[-\d]+" %)
                      (parse-int %)
                      (keyword %))
                   args)]
    {:op   (keyword op)
     :args args}))

(def input (mapv parse-input-line (str/split-lines input-string)))

(defn get-value [registers value]
  (if (number? value)
    value
    (get registers value 0)))

(defn update-register [state k f]
  (update-in state [:registers k] (fnil f 0)))

(defn step [state program]
  (let [{:keys [:ip :registers :mult-count]} state]
    (if (and (>= ip 0) (< ip (count program)))
      (let [{:keys [:op :args]} (program ip)
            [arg0 arg1] args
            state (case op
                    :set (assoc-in state [:registers arg0] (get-value registers arg1))
                    :sub (update-register state arg0 #(- % (get-value registers arg1)))
                    :mul (-> state
                             (update-register arg0 #(* % (get-value registers arg1)))
                             (update :mult-count inc))
                    :jnz (let [value0 (get-value registers arg0)]
                           (if (not (zero? value0))
                             (assoc state :ip (dec (+ ip (get-value registers arg1))))
                             state)))
            state (update state :ip inc)]
        state))))

(defn part1 []
  (let [initial-state {:ip         0
                       :registers  {}
                       :mult-count 0}]
    (:mult-count (last (take-while identity (iterate #(step % input) initial-state))))))

(defn part2 []
  (let [lower (+ (* 84 100) 100000)
        upper (+ lower 17000)
        primes (set (sieve (inc upper)))]
    (count
      (filter
        (comp not primes)
        (range lower (inc upper) 17)))))
