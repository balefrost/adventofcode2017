(ns day8
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day8_input.txt")))

(def input-lines (str/split-lines input-string))

(def instr-regex #"^(\w+) (inc|dec) (-?\d+) if (\w+) (>|<|>=|<=|==|!=) (-?\d+)$")

(defn parse-line [line]
  (let [[_ register opcode amount cond-reg cond-op cond-amount] (re-matches instr-regex line)
        opcode-fn (case opcode
                    "inc" +
                    "dec" -)
        amount (advent-util/parse-int amount)
        cond-op (case cond-op
                  ">" >
                  "<" <
                  ">=" >=
                  "<=" <=
                  "==" =
                  "!=" not=)
        cond-amount (advent-util/parse-int cond-amount)
        operation (fn [registers]
                    (assoc registers register (opcode-fn (get registers register 0) amount)))
        condition (fn [registers]
                    (cond-op (get registers cond-reg 0) cond-amount))]
    {:operation operation
     :condition condition}))

(def input (map parse-line input-lines))

(defn run-instruction [registers instruction]
  (let [{:keys [:operation :condition]} instruction]
    (if (condition registers)
      (operation registers)
      registers)))

(defn part1 []
  (apply
    max
    (vals
      (reduce
        run-instruction
        {}
        input))))

(defn part2 []
  (let [all-states (reductions
                     run-instruction
                     {}
                     input)]
    (apply
      max
      (mapcat vals all-states))))
