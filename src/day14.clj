(ns day14
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [day10 :as day10])
  (:use advent-util))

(def input "hxtvlmkl")

(def hashes
  (map
    #(day10/compute-knot-hash (str input "-" %))
    (range 128)))

(def hex-char-patterns
  {\0 [0 0 0 0]
   \1 [0 0 0 1]
   \2 [0 0 1 0]
   \3 [0 0 1 1]
   \4 [0 1 0 0]
   \5 [0 1 0 1]
   \6 [0 1 1 0]
   \7 [0 1 1 1]
   \8 [1 0 0 0]
   \9 [1 0 0 1]
   \a [1 0 1 0]
   \b [1 0 1 1]
   \c [1 1 0 0]
   \d [1 1 0 1]
   \e [1 1 1 0]
   \f [1 1 1 1]})

(defn hex-string-to-bits [str]
  (mapcat hex-char-patterns str))

(defn part1 []
  (->> hashes
       (mapcat hex-string-to-bits)
       (filter #(= 1 %))
       (count)))

(defn adjacent-coords [coord]
  (let [[x y] coord]
    (for [[dx dy] [[-1 0] [1 0] [0 -1] [0 1]]
          :let [x (+ x dx)
                y (+ y dy)]
          :when (>= x 0)
          :when (>= y 0)
          :when (<= x 127)
          :when (<= y 127)]
      [x y])))

(def all-coords
  (for [y (range 128)
        x (range 128)]
    [x y]))

(defn walk-region [starting-coord used-coords]
  (letfn [(helper [remaining-coords seen]
            (lazy-seq
              (if-let [[coord & remaining-coords] (seq remaining-coords)]
                (if (and
                      (not (seen coord))
                      (used-coords coord))
                  (cons coord (helper
                                (apply conj remaining-coords (adjacent-coords coord))
                                (conj seen coord)))
                  (helper remaining-coords seen)))))]
    (helper [starting-coord] #{})))


(defn part2 []
  (let [used-coords (->> hashes
                         (mapcat hex-string-to-bits)
                         (map vector all-coords)
                         (filter #(= 1 (second %)))
                         (map first)
                         (set))]
    (loop [count 0
           used-coords used-coords]
      (if (empty? used-coords)
        count
        (let [[coord] (seq used-coords)
              region (walk-region coord used-coords)]
          (recur
            (inc count)
            (apply disj used-coords region)))))))
