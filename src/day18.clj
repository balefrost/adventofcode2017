(ns day18
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (str/trim (slurp (io/resource "day18_input.txt"))))

(defn parse-input-line [line]
  (let [[_ op args] (re-matches #"^(\w+?) (.*)$" line)
        args (re-seq #"[^ ]+" args)
        args (mapv #(if (re-matches #"[-\d]+" %)
                      (parse-int %)
                      (keyword %))
                   args)]
    {:op   (keyword op)
     :args args}))

(def input (mapv parse-input-line (str/split-lines input-string)))

(defn get-value [registers value]
  (if (number? value)
    value
    (get registers value 0)))

(defn rcv-sound [state args]
  (let [[arg1] args
        {:keys [:registers :last-sound]} state
        value1 (get-value registers arg1)]
    {:state   (if (zero? value1)
                state
                (assoc state :last-recovered last-sound))
     :message nil}))

(defn snd-sound [state args]
  (let [[arg1] args
        {:keys [:registers :last-sound]} state
        value1 (get-value registers arg1)]
    {:state   (assoc state :last-sound value1)
     :message nil}))

(defn step-machine [program state rcv-imp snd-imp]
  (let [{:keys [:ip :registers :last-sound]} state
        instruction (get program ip)
        {:keys [:op :args]} instruction
        [arg1 arg2] args
        result (case op
                 :snd (snd-imp state args)
                 :set (let [value2 (get-value registers arg2)]
                        {:state   (assoc-in state [:registers arg1] value2)
                         :message nil})
                 :add (let [value1 (get-value registers arg1)
                            value2 (get-value registers arg2)]
                        {:state   (assoc-in state [:registers arg1] (+ value1 value2))
                         :message nil})
                 :mul (let [value1 (get-value registers arg1)
                            value2 (get-value registers arg2)]
                        {:state   (assoc-in state [:registers arg1] (* value1 value2))
                         :message nil})
                 :mod (let [value1 (get-value registers arg1)
                            value2 (get-value registers arg2)]
                        {:state   (assoc-in state [:registers arg1] (rem value1 value2))
                         :message nil})
                 :rcv (rcv-imp state args)
                 :jgz (let [value1 (get-value registers arg1)
                            value2 (get-value registers arg2)]
                        {:state   (if (> value1 0)
                                    (assoc state :ip (+ ip (dec value2)))
                                    state)
                         :message nil}))
        result (update-in result [:state :ip] inc)]
    result))

(defn execute-program [program]
  (letfn [(helper [state]
            (lazy-seq
              (if (and
                    (>= (:ip state) 0)
                    (< (:ip state) (count program)))
                (let [result (step-machine program state rcv-sound snd-sound)
                      state (:state result)]
                  (cons state (helper state))))))]
    (helper {:ip             0
             :registers      {}
             :last-sound     nil
             :last-recovered nil})))

(defn part1 []
  (->> input
       (execute-program)
       (map :last-recovered)
       (filter identity)
       (first)))

(defn rcv-queue [state args]
  (let [[arg1] args
        {:keys [:registers :my-queue]} state]
    {:state   (if-let [[hd & rest-queue] (seq my-queue)]
                (-> state
                    (assoc-in [:registers arg1] hd)
                    (assoc :my-queue (vec rest-queue)))
                (update state :ip dec))
     :message nil}))

(defn snd-queue [state args]
  (let [[arg1] args
        {registers :registers} state
        value1 (get-value registers arg1)]
    {:state   (update state :num-sends inc)
     :message value1}))

(defn is-deadlock? [state1 state2 program]
  (let [[op1 op2] (map #(:op (program (:ip %))) [state1 state2])
        [queue1 queue2] (map :my-queue [state1 state2])]
    (and
      (= op1 :rcv)
      (= op2 :rcv)
      (empty? queue1)
      (empty? queue2))))

(defn step-part2 [state program]
  (let [[state0 state1] state]
    (if (is-deadlock? state0 state1 program)
      (do
        (println "deadlock")
        nil)
      (let [
            {state0 :state message :message} (step-machine program state0 rcv-queue snd-queue)
            state1 (if message
                     (update state1 :my-queue #(conj % message))
                     state1)
            {state1 :state message :message} (step-machine program state1 rcv-queue snd-queue)
            state0 (if message
                     (update state0 :my-queue #(conj % message))
                     state0)]
        [state0 state1]))))

(defn iterate-while-not-nil [f x]
  (take-while
    #(not (nil? %))
    (iterate f x)))

(defn part2 []
  (let [initial-state [{:ip        0
                        :registers {:p 0}
                        :num-sends 0
                        :my-queue  []}
                       {:ip        0
                        :registers {:p 1}
                        :num-sends 0
                        :my-queue  []}]]
    (let [[state0 state1] (last (iterate-while-not-nil #(step-part2 % input) initial-state))]
      (:num-sends state1))))

