(ns day21
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day21_input.txt")))

(def line-re #"^(.*) => (.*)$")

(defn flip-x [pattern]
  (map (comp vec reverse) pattern))

(defn rotate-clockwise [pattern]
  (flip-x (apply map vector pattern)))

(defn all-orientations [pattern]
  (concat
    (take 4 (iterate rotate-clockwise pattern))
    (take 4 (iterate rotate-clockwise (flip-x pattern)))))

(defn parse-line [line]
  (let [[_ pre post] (re-matches line-re line)
        pre-lines (str/split pre #"/")
        post-lines (str/split post #"/")]
    {:from (mapv vec pre-lines)
     :to   (mapv vec post-lines)}))

(defn print-pattern [pattern]
  (dorun
    (for [row pattern]
      (println (str/join row)))))

(def input (map parse-line (str/split-lines input-string)))

(def patterns
  (into
    {}
    (mapcat
      #(let [{:keys [from to]} %]
         (for [orientation (set (all-orientations from))]
           [orientation to]))
      input)))

(def initial-picture (mapv vec [".#."
                                "..#"
                                "###"]))

(defn split-into-chunks [picture size]
  (let [partition-fn (partial partition size)
        col-chunks (mapv partition-fn picture)
        row-chunks (partition-fn col-chunks)
        result (mapcat #(apply map vector %) row-chunks)]
    result))

(defn join-chunks [chunks chunks-per-row]
  (let [row-chunks (partition chunks-per-row chunks)
        joined-row-chunks (for [chunks-to-join row-chunks
                                :let [foo (apply map concat chunks-to-join)]]
                            foo)]
    (apply concat joined-row-chunks)))


(defn step-picture [current]
  (let [size (count current)
        split-size (if (zero? (mod size 2))
                     2
                     3)
        chunks-per-row (/ size split-size)
        chunks (split-into-chunks current split-size)
        replacements (map patterns chunks)
        result (join-chunks replacements chunks-per-row)]
    result))

(defn solve [iterations]
  (count
    (filter
      #{\#}
      (apply
        concat
        (nth
          (iterate step-picture initial-picture)
          iterations)))))

(defn part1 []
  (solve 5))

(defn part2 []
  (solve 18))
