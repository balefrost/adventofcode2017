(ns day7
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day7_input.txt")))

(def line-pattern #"^([a-z]+) \((\d+)\)(?: -> ([a-z]+(?:, [a-z]+)*))?$")

(def word-pattern #"[a-z]+")

(defn parse-line [line]
  (let [[_ name weight rest] (re-matches line-pattern line)
        children (if (seq rest) (vec (re-seq word-pattern rest)) [])]
    {:name     name
     :weight   (advent-util/parse-int weight)
     :children children}))

(def input
  (mapv
    parse-line
    (str/split-lines input-string)))

(def input-map
  (into {} (map
             (fn [x]
               [(:name x)
                x])
             input)))

(defn find-root [config-map]
  (let [all-children (set (mapcat :children (vals config-map)))]
    (first (filter (comp not all-children) (keys input-map)))))

(defn part1 []
  (find-root input-map))

(defn postorder-traversal [config-map root]
  (lazy-seq
    (let [entry (config-map root)
          child-names (:children entry)]
      (concat
        (mapcat #(postorder-traversal config-map %) child-names)
        [entry]))))

(defn compute-weights [config-map root]
  (reduce
    (fn [weights {:keys [:name :weight :children]}]
      (let [child-weights (map weights children)
            total-weight (+
                           weight
                           (apply + child-weights))]
        (assoc weights name total-weight)))
    {}
    (postorder-traversal config-map root)))

(defn compute-neighbor-map [input-map]
  (into
    {}
    (for [{:keys [:children]} (vals input-map)
          c children]
      [c (filter #(not= c %) children)])))

(defn part2 []
  (let [root (find-root input-map)
        weights (compute-weights input-map root)
        neighbors (compute-neighbor-map input-map)]
    (first
      (for [entry (postorder-traversal input-map root)
            :let [neighbor-names (neighbors (:name entry))]
            :when (> (count neighbor-names) 0)
            :let [my-weight (weights (:name entry))
                  neighbor-weights (map weights neighbor-names)]
            :when (apply = neighbor-weights)
            :when (not= my-weight (first neighbor-weights))]
        (first neighbor-weights)))))
