(ns day9
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (str/trim (slurp (io/resource "day9_input.txt"))))

(def read-garbage)

(defn read-group [coll]
  (loop [children []
         coll (rest coll)]
    (case (first coll)
      \} [{:type     :group
           :children children}
          (rest coll)]
      \{ (let [[grp coll] (read-group coll)]
           (recur
             (conj children grp)
             coll))
      \< (let [[garbage coll] (read-garbage coll)]
           (recur
             (conj children garbage)
             coll))
      (recur
        children
        (rest coll)))))

(defn read-garbage [coll]
  (loop [chars []
         coll (rest coll)]
    (case (first coll)
      \> [{:type  :garbage
           :chars (apply str chars)}
          (rest coll)]
      \! (recur
           chars
           (drop 2 coll))
      (recur
        (conj chars (first coll))
        (rest coll)))))

(defn read-input [seq]
  (first
    (case (first seq)
      \{ (read-group seq)
      \< (read-garbage seq))))

(defn score-group [grp score]
  (let [children (:children grp)]
    (+ score
       (apply
         +
         (->> children
              (filter #(= (:type %) :group))
              (map #(score-group % (inc score))))))))

(defn score-garbage [grp]
  (let [children (:children grp)
        group-children (filter (comp #{:group} :type) children)
        garbage-children (filter (comp #{:garbage} :type) children)]
    (+
      (apply
        +
        (map score-garbage group-children))
      (apply
        +
        (map #(count (:chars %)) garbage-children)))))

(defn part1 []
  (score-group (read-input input-string) 1))

(defn part2 []
  (score-garbage (read-input input-string)))