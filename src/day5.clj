(ns day5
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day5_input.txt")))

(def input
  (mapv
    advent-util/parse-int
    (str/split-lines input-string)))

(defn in-range [n low high]
  (and
    (>= n low)
    (< n high)))

(defn step [state offset-update-fn]
  (let [{:keys [index offsets]} state]
    (if (in-range index 0 (count offsets))
      (let [offset (offsets index)
            index1 (+ index offset)
            offsets1 (update offsets index offset-update-fn)]
        {:index   index1
         :offsets offsets1})
      nil)))

(defn solve [offset-update-fn offsets]
  (let [initial-state {:index 0 :offsets offsets}
        states (take-while identity (iterate #(step % offset-update-fn) initial-state))]
    (dec (count states))))

(defn part1 []
  (solve
    inc
    input))

(defn part2 []
  (solve
    (fn [offset]
      (cond
        (>= offset 3) (dec offset)
        :else (inc offset)))
    input))
