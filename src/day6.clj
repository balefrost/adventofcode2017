(ns day6
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day6_input.txt")))

(def input
  (mapv
    advent-util/parse-int
    (str/split input-string #"\s+")))

(defn compare-bank [[index1 bank1] [index2 bank2]]
  (let [x (compare bank2 bank1)]
    (if (= x 0)
      (compare index1 index2)
      x)))

(defn select-redistribution-index [banks]
  (let [banks-with-index (map #(vector %1 %2) (range) banks)
        ordered-banks (sort compare-bank banks-with-index)
        [best-bank-index _] (first ordered-banks)]
    best-bank-index))

(defn redistribute-at-index [banks index]
  (let [num-banks (count banks)
        all-indices (cycle (range num-banks))
        count-to-redistribute (get banks index)
        minimum-per-bank (quot count-to-redistribute num-banks)
        extra-banks (rem count-to-redistribute num-banks)
        extra-bank-indices (take extra-banks (drop (inc index) all-indices))
        banks1 (assoc banks index 0)
        banks2 (mapv #(+ % minimum-per-bank) banks1)
        banks3 (reduce #(update %1 %2 inc) banks2 extra-bank-indices)]
    banks3))

(defn redistribute [banks]
  (let [redistribution-index (select-redistribution-index banks)
        redistributed (redistribute-at-index banks redistribution-index)]
    redistributed))

(defn split-unique-prefix [coll]
  (loop [coll coll
         seen #{}
         prefix []]
    (if-let [[hd & tl] (seq coll)]
      (if (seen hd)
        [prefix coll]
        (recur
          tl
          (conj seen hd)
          (conj prefix hd)))
      [prefix []])))


(defn part1 []
  (let [redistributions (iterate redistribute input)
        [prefix _] (split-unique-prefix redistributions)]
    (count prefix)))

(defn count-while [pred coll]
  (loop [coll coll
         count 0]
    (if-let [[hd & tl] (seq coll)]
      (if (pred hd)
        (recur (rest coll) (inc count))
        count)
      count)))

(defn part2 []
  (let [redistributions (iterate redistribute input)
        [_ [first-unique & rest]] (split-unique-prefix redistributions)]
    (inc (count-while #(not= % first-unique) rest))))
