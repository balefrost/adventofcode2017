(ns day22
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day22_input.txt")))

(def input
  (let [lines (str/split-lines input-string)
        height (count lines)
        width (count (first lines))
        x-offset (- (quot (dec width) 2))
        y-offset (- (quot (dec height) 2))]
    (into
      {}
      (for [[y line] (map vector (range) lines)
            [x char] (map vector (range) line)]
        [[(+ x x-offset) (+ y y-offset)] (case char \# :infected \. :clean)]))))

(defn turn-right [dir]
  (let [[dx dy] dir]
    [(- dy) dx]))

(defn turn-left [dir]
  (let [[dx dy] dir]
    [dy (- dx)]))

(defn step-grid [update-dir update-cell state]
  (let [{:keys [:pos :dir :grid :infections-caused]} state
        cell (get grid pos :clean)
        dir (update-dir cell dir)
        new-cell (update-cell cell)
        grid (assoc grid pos new-cell)
        pos (mapv + pos dir)]
    {:pos               pos
     :dir               dir
     :grid              grid
     :infections-caused (if (= new-cell :infected) (inc infections-caused) infections-caused)}))

(defn print-grid [state]
  (let [{:keys [:pos :grid :dir]} state
        xs (map first (keys grid))
        ys (map second (keys grid))
        min-x (apply min xs)
        min-y (apply min ys)
        max-x (apply max xs)
        max-y (apply max ys)]
    (println "dir" dir)
    (dorun
      (for [y (range min-y (inc max-y))]
        (let [line (apply str (for [x (range min-x (inc max-x))
                                    :let [ch (if (get grid [x y] false)
                                               \#
                                               \.)]]
                                (if (= [x y] pos)
                                  (str \[ ch)
                                  (if (= [(dec x) y] pos)
                                    (str \] ch)
                                    (str \space ch)))))]
          (println line))))))




(def initial-state {:pos               [0 0]
                    :dir               [0 -1]
                    :grid              input
                    :infections-caused 0})

(defn solve [initial-state iterations update-dir update-cell]
  (:infections-caused (->>
                        initial-state
                        (iterate
                          (partial
                            step-grid
                            (fn [cell dir]
                              ((update-dir cell) dir))
                            update-cell))
                        (drop iterations)
                        (first))))


(defn part1 []
  (solve
    initial-state
    10000
    {:infected turn-right
     :clean    turn-left}
    {:clean    :infected
     :infected :clean}))

(defn part2 []
  (solve
    initial-state
    10000000
    {:infected turn-right
     :clean    turn-left
     :weakened identity
     :flagged  (partial mapv -)}
    {:clean    :weakened
     :weakened :infected
     :infected :flagged
     :flagged  :clean}))
