(ns day4
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day4_input.txt")))

(def input-lines (str/split-lines input-string))

(def input-words (map #(str/split % #"\s+") input-lines))

(defn check-input-line [words]
  (let [grouped (group-by identity words)]
    (every? #(= 1 (count (second %))) grouped)))

(defn part1 []
  (count
    (filter
      check-input-line
      input-words)))

(defn sort-word [word]
  (sort word))

(defn sort-words [words]
  (map sort-word words))

(defn part2 []
  (count
    (filter
      check-input-line
      (map sort-words input-words))))