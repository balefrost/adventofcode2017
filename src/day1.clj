(ns day1
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day1_input.txt")))

(def input-digits
  (->> input-string
       (str/trim)
       (map str)
       (mapv advent-util/parse-int)))


(defn rot-input-digits [input n]
  (take
    (count input)
    (concat (drop n input) input)))

(defn solve [input n]
  (apply +
         (map
           (fn [a b]
             (if (= a b)
               a
               0))
           input
           (rot-input-digits input n))))

(defn part1 []
  (solve input-digits 1))

(defn part2 []
  (solve input-digits (/ (count input-digits) 2)))
