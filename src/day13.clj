(ns day13
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (str/trim (slurp (io/resource "day13_input.txt"))))

(def input-line-regex #"^(\d+): (\d+)$")

(defn parse-input-line [line]
  (let [[_ layer range] (re-matches input-line-regex line)]
    {:layer (advent-util/parse-int layer)
     :range (advent-util/parse-int range)}))

(def input (map parse-input-line (str/split-lines input-string)))

(defn test-layer-config-with-delay [delay config]
  (let [{layer :layer range :range } config
        total-delay (+ delay layer)
        collides (zero? (mod total-delay (+ range range -2)))]
    collides))

(defn part1 []
  (apply
    +
    (for [config input
          :let [{layer :layer range :range} config
                collides (test-layer-config-with-delay 0 config)]]
      (if collides
        (* layer range)
        0))))

(defn test-with-delay [delay input]
  (every?
    #(not (test-layer-config-with-delay delay %))
    input))

(defn part2 []
  (let [successful-delays (pmap
                            (fn [delay]
                              (if (test-with-delay delay input)
                                delay))
                            (range))]
    (some identity successful-delays)))
