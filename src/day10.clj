(ns day10
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (str/trim (slurp (io/resource "day10_input.txt"))))

(def input
  (map
    advent-util/parse-int
    (str/split input-string #",")))

(defn twist-pairs [position twist-length list-length]
  (letfn [(helper [remaining lower upper]
            (lazy-seq
              (if (> remaining 0)
                (cons
                  [lower upper]
                  (helper
                    (dec remaining)
                    (mod (inc lower) list-length)
                    (mod (dec upper) list-length))))))]
    (let [position (mod position list-length)
          upper (mod (+ position twist-length) list-length)]
      (helper
        (quot twist-length 2)
        (mod position list-length)
        (mod (dec upper) list-length)))))

(defn apply-twists [list twists]
  (letfn [(twist-list [list position length]
            (reduce
              (fn [acc [lower upper]]
                (let [lowerVal (get list lower)
                      upperVal (get list upper)]
                  (-> acc
                      (assoc! lower upperVal)
                      (assoc! upper lowerVal))))
              list
              (twist-pairs position length (count list))))]
    (loop [list (transient (vec list))
           position 0
           skip 0
           twists twists]
      (if (empty? twists)
        (persistent! list)
        (let [[twist & twists] twists
              twisted (twist-list list position twist)]
          (recur
            twisted
            (+ position twist skip)
            (inc skip)
            twists))))))

(defn part1 []
  (let [[a b] (apply-twists (range 256) input)]
    (* a b)))

(defn compute-seed-bytes [string]
  (concat
    (map int string)
    [17 31 73 47 23]))

(defn compute-sparse-hash [bytes]
  (apply-twists
    (range 256)
    (apply concat (repeat 64 bytes))))

(defn compute-dense-hash [sparse-hash]
  (->> sparse-hash
       (partition 16)
       (map #(apply bit-xor %))
       (byte-array)
       (bytes-to-hex)))

(defn compute-knot-hash [string]
  (-> string
      (compute-seed-bytes)
      (compute-sparse-hash)
      (compute-dense-hash)))

(defn part2 []
  (compute-knot-hash (str/trim input-string)))