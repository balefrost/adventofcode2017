(ns day11
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (str/trim (slurp (io/resource "day11_input.txt"))))

(defn parse-input [input-string]
  (mapv
    #(case %
       "n" :n
       "ne" :ne
       "se" :se
       "s" :s
       "sw" :sw
       "nw" :nw)
    (str/split input-string #",")))

; OK, so it's possible to represent a hex grid on an X/Y grid by treating four of the edges - in my case
; NE, SE, SW, and NW - as orthogonal connections. So if I move in that direction, I change either X or Y. I can
; then handle N and S as diagonal moves - I update both X and Y. This makes some amount of sense - if I go NE
; then NW, I end up directly N of where I started.
;
; This makes it tricky to find distance, but we'll deal with that below.

(def move-deltas
  {:n  [1 1]
   :ne [1 0]
   :se [0 -1]
   :s  [-1 -1]
   :sw [-1 0]
   :nw [0 1]})

(def input (parse-input input-string))

(defn take-step [position move]
  (let [deltas (move-deltas move)]
    (mapv + position deltas)))

(defn take-steps [position steps]
  (reduce
    take-step
    position
    steps))

; If I find myself in the first quadrant, then I can move south some number of steps to arrive at either x = 0 or y = 0.
; At that point, I can follow the X or Y axis to arrive back at the origin. Similar logic exists for the third
; quadrant - I can move N to arrive at one of those axes. This will be the smallest distance, since N and S moves affect
; my Manhattan distance faster than other moves (since they affect both X and Y).
;
; However, if I find myself in the second or fourth quadrant, then moves N or S will send me further away from the
; origin.

(defn calc-hex-distance [position]
  (let [[x y] position]
    (cond
      (and (> x 0) (> y 0)) (let [mincoord (min x y)
                                  result (- (+ x y) mincoord)]
                              result)
      (and (< x 0) (< y 0)) (let [maxcoord (max x y)
                                  result (- maxcoord x y)]
                              result)
      :else (+ (advent-util/absolute-value x) (advent-util/absolute-value y)))))

(defn solve1 [move-string]
  (let [moves (parse-input move-string)
        position (take-steps [0 0] moves)
        distance (calc-hex-distance position)]
    distance))

(defn part1 []
  (solve1 input-string))

(defn part2 []
  (let [moves (parse-input input-string)
        positions (reductions take-step [0 0] moves)
        distances (map calc-hex-distance positions)]
    (apply max distances)))
