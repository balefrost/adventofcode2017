(ns day16
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (str/trim (slurp (io/resource "day16_input.txt"))))

(def token-re #"s(\d+)|x(\d+)/(\d+)|p(\w+)/(\w+)")

(def letters [\a \b \c \d \e \f \g \h \i \j \k \l \m \n \o \p])

(defn parse-token [token]
  (let [[_ spin x1 x2 p1 p2] (re-matches token-re token)]
    (cond
      spin {:type :spin :spin (parse-int spin)}
      x1 {:type :exchange :x1 (parse-int x1) :x2 (parse-int x2)}
      p1 {:type :partner :p1 (first p1) :p2 (first p2)})))

(def input (map parse-token (str/split input-string #",")))

(defn swap-indices [coll i1 i2]
  (-> coll
      (assoc i1 (coll i2))
      (assoc i2 (coll i1))))

(defn execute-move [state move]
  (case (:type move)
    :spin (let [{spin :spin} move]
            (vec (concat (take-last spin state)
                         (take (- (count state) spin) state))))
    :exchange (let [{x1 :x1 x2 :x2} move]
                (swap-indices state x1 x2))
    :partner (let [{p1 :p1 p2 :p2} move
                   i1 (.indexOf state p1)
                   i2 (.indexOf state p2)]
               (swap-indices state i1 i2))))

(defn part1 []
  (apply str
         (reduce
           execute-move
           letters
           input)))

(def cycle-limit 1000000000)

(defn part2 []
  (loop [i 0
         state letters
         seen #{}
         log []]
    (if (= i cycle-limit)
      (apply str state)
      (if (seen state)
        (let [pre-cycle (take-while #(not= state %) log)
              cycle (drop (count pre-cycle) log)
              into-cycle (mod
                           (- cycle-limit (count pre-cycle))
                           (count cycle))]
          (apply str (nth cycle into-cycle)))
        (recur
          (inc i)
          (reduce execute-move state input)
          (conj seen state)
          (conj log state))))))
