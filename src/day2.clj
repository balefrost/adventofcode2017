(ns day2
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:use advent-util))

(def input-string (slurp (io/resource "day2_input.txt")))

(def input-lines (str/split-lines input-string))

(def numbers
  (map
    (fn [line]
      (map
        advent-util/parse-int
        (str/split line #"\s+")))
    input-lines))

(defn difference-row [row]
  (let [x (apply min row)
        y (apply max row)]
    (- y x)))

(defn find-divisors [row]
  (let [sorted (sort (comp - compare) row)
        tails (take-while seq (iterate rest sorted))]
    (first
      (for [[x & tl] tails
            y tl
            :when (zero? (mod x y))]
        (quot x y)))))

(defn part1 []
  (apply + (map difference-row numbers)))

(defn part2 []
  (apply + (map find-divisors numbers)))